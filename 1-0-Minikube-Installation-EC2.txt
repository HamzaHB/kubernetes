######################################################
# Commands-lines to install Minikube on EC2 instance #
######################################################

# OS: UBUNTU 22.04 | 2 CPU | 8GB RAM | 30GB DISK

#STEP1: Update system

sudo apt update
sudo apt install apt-transport-https
sudo apt upgrade

#STEP2: We have to install a hyperviser, in our case: Docker

#STEP3: Download minikube on UBUNTU

wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
chmod +x minikube-linux-amd64
sudo mv minikube-linux-amd64 /usr/local/bin/minikube

minikube version

#STEP4: Install Kubectl on UBUNTU

curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl

chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubect
kubectl version -o json  --client

#STEP5: Starting minikube on UBUNTU

minikube start

#STEP6: Minikube Basic operations
#To check cluster status, run:

kubectl cluster-info

#Note that Minikube configuration file is located under ~/.minikube/machines/minikube/config.json

#To View Config, use:

kubectl config view

#To check running nodes:

kubectl get nodes

#Access minikube VM using ssh:

minikube ssh
sudo su -

#To stop a running local kubernetes cluster, run:

minikube stop

#To delete a local kubernetes cluster, use:

minikube delete




